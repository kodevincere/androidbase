package com.kodevincere.androidbase.base.launcher;

/**
 * Created by James
 */
public enum LauncherViews {
    NONE;


    private Class cls;

    LauncherViews(){}

    LauncherViews(Class cls){
        this.cls = cls;
    }

    public Class getCls() {
        return cls;
    }
}
