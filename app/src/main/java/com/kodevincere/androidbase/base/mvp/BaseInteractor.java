package com.kodevincere.androidbase.base.mvp;

import android.content.Context;

public abstract class BaseInteractor<P extends BasePresenterModel> {

    protected P presenterModel;
    protected Context context;

    public BaseInteractor(P presenterModel, Context context) {
        this.presenterModel = presenterModel;
        this.context = context;
    }
}