package com.kodevincere.androidbase.base.mvp;

import android.content.Context;
import android.content.Intent;

import com.kodevincere.androidbase.base.ui.BaseViewModel;
import com.kodevincere.androidbase.base.util.NetworkState;

public abstract class BasePresenter<V extends BaseViewModel, I extends BaseInteractor>{

    protected V viewModel;
    protected I interactor;
    protected Context context;
    private boolean callViewModel = true;

    public BasePresenter(V viewModel) {
        this.viewModel = viewModel;
        this.context = viewModel.getViewContext().getApplicationContext();
        interactor = startInteractor();
    }

    public abstract I startInteractor();

    public void create(){}

    public void start(){}

    public void resume() {
        callViewModel = true;
    }

    public void activityResult(int requestCode, int resultCode, Intent intentData) {}

    public void pause() {}

    public void stop() {}

    public void destroy() {
        callViewModel = false;
    }

    public boolean checkInternetConnection(){
        return NetworkState.isNetworkConnected(context);
    }

    public boolean canCallView(){
        return callViewModel;
    }
}
