package com.kodevincere.androidbase.base.recycleview;

/**
 * Created by James
 */
public interface OnActionAdapter {
    void onActionAdapter(ActionPack actionPack);
}
