package com.kodevincere.androidbase.base.ui;

import android.content.Context;

public interface BaseViewModel {
    Context getViewContext();
}
